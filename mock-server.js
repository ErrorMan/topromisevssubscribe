const express = require('express');
const app = express();

const data = {
  users: [
    "Liam", "Olivia",
    "Noah", "Emma",
    "Oliver", "Ava",
    "William", "Sophia",
    "Elijah", "Isabella",
    "James", "Charlotte",
    "Benjamin", "Amelia",
    "Lucas", "Mia",
    "Mason", "Harper",
    "Ethan", "Evelyn"
  ],
  timer: 0
};

app.get('/api/mock-data', (req, res) => {
  const timeout = +req.query.timeout;
  const timer = timeout;
  res.set({'Access-Control-Allow-Origin': '*'});
  setTimeout(() => {
    res.send({...data, ...{timer: timer}});
  }, timeout)
});

app.listen(3000);
