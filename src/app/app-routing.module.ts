import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { PromiseAwaitComponent } from './pages/promise-await/promise-await.component';
import { UnsubscribeComponent } from './pages/unsubscribe/unsubscribe.component';
import { AsyncPipeComponent } from './pages/async-pipe/async-pipe.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'promise-await',
        component: PromiseAwaitComponent
      },
      {
        path: 'unsubscribe',
        component: UnsubscribeComponent
      },
      {
        path: 'async-pipe',
        component: AsyncPipeComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload'})],
  exports: [RouterModule],
})
export class AppRoutingModule { }
