import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-async-pipe',
  templateUrl: './async-pipe.component.html'
})
export class AsyncPipeComponent implements OnInit, OnDestroy {

  public serviceCode = '  getDataRx(timeout = 0): Observable<any> {\n' +
    '    return this.http.get(`${this.baseUrl}/api/mock-data?timeout=${timeout}`);\n' +
    '  }';

  public getDataCode = 'getData(): void {\n' +
    '    this.data$ = this.dataService.getDataRx(this.responseTimeout)\n' +
    '      .pipe(tap(() => {\n' +
    '        this.toastr.success(\'Данные получены (вывод сообщения инициируется в методе компонента AsyncPipeComponent)\');\n' +
    '      }));\n' +
    '  }';

  public scenariosCode = {
    leave: '() => {\n' +
      '      this.responseTimeout = 6000;\n' +
      '      this.getData();\n' +
      '      setTimeout(() => {\n' +
      '        this.goToHome();\n' +
      '      }, 2000);\n' +
      '    }',
    leaveAndAgain: '() => {\n' +
      '      this.responseTimeout = 6000;\n' +
      '      this.getData();\n' +
      '      setTimeout(() => {\n' +
      '        this.goToHome();\n' +
      '      }, 2000);\n' +
      '      setTimeout(() => {\n' +
      '        this.goToCurrentPage();\n' +
      '      }, 3000);\n' +
      '    }',
    clicker: '() => {\n' +
      '      this.responseTimeout = 2000;\n' +
      '      this.getData();\n' +
      '      setTimeout(() => {\n' +
      '        for (let i = 0; i < 4; i++) {\n' +
      '          this.getData();\n' +
      '        }\n' +
      '      }, 2000);\n' +
      '      setTimeout(() => {\n' +
      '        this.goToHome();\n' +
      '      }, 4000);\n' +
      '    }'
  };

  /** данные полученные с бакенда */
  public data$ = null;

  /** время через которое сервер ответит */
  public responseTimeout = 3000;

  public scenarios = {
    leave: () => {
      this.responseTimeout = 6000;
      this.getData();
      setTimeout(() => {
        this.goToHome();
      }, 2000);
    },
    leaveAndAgain: () => {
      this.responseTimeout = 6000;
      this.getData();
      setTimeout(() => {
        this.goToHome();
      }, 2000);
      setTimeout(() => {
        this.goToCurrentPage();
      }, 3000);
    },
    clicker: () => {
      this.responseTimeout = 2000;
      this.getData();
      setTimeout(() => {
        for (let i = 0; i < 4; i++) {
          this.getData();
        }
      }, 2000);
      setTimeout(() => {
        this.goToHome();
      }, 4500);
    }
  };

  constructor(private dataService: DataService,
              private toastr: ToastrService,
              private router: Router
  ) {
  }

  getData(): void {
    this.data$ = this.dataService.getDataRx(this.responseTimeout)
      .pipe(tap(() => {
        this.toastr.success('Данные получены (вывод сообщения инициируется в методе компонента AsyncPipeComponent)');
      }));
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.toastr.warning('сработал ngOnDestroy компонента AsyncPipeComponent!');
  }


  goToHome(): void {
    this.router.navigate(['/']);
  }

  goToCurrentPage(): void {
    this.router.navigate(['/async-pipe']);
  }

}
