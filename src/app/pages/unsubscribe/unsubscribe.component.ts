import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AutoUnsubscribe } from '@ngx-esphere/common';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-subscribe',
  templateUrl: './unsubscribe.component.html'
})
@AutoUnsubscribe({unsubscribeProps: ['subs']})
export class UnsubscribeComponent implements OnInit, OnDestroy {

  public serviceCode = '  getDataRx(timeout = 0): Observable<any> {\n' +
    '    return this.http.get(`${this.baseUrl}/api/mock-data?timeout=${timeout}`);\n' +
    '  }';

  public scenariosCode = {
    leave: '() => {\n' +
      '      this.responseTimeout = 6000;\n' +
      '      this.getData();\n' +
      '      setTimeout(() => {\n' +
      '        this.goToHome();\n' +
      '      }, 2000);\n' +
      '    }',
    leaveAndAgain: '() => {\n' +
      '      this.responseTimeout = 6000;\n' +
      '      this.getData();\n' +
      '      setTimeout(() => {\n' +
      '        this.goToHome();\n' +
      '      }, 2000);\n' +
      '      setTimeout(() => {\n' +
      '        this.goToCurrentPage();\n' +
      '      }, 3000);\n' +
      '    }',
    clicker: '() => {\n' +
      '      this.responseTimeout = 2000;\n' +
      '      this.getData();\n' +
      '      setTimeout(() => {\n' +
      '        for (let i = 0; i < 4; i++) {\n' +
      '          this.getData();\n' +
      '        }\n' +
      '      }, 2000);\n' +
      '      setTimeout(() => {\n' +
      '        this.goToHome();\n' +
      '      }, 4000);\n' +
      '    }'
  };

  public getDataCode = 'getData(): void {\n' +
    '    this.data = null;\n' +
    '    this.subs.sink = this.dataService.getDataRx(this.responseTimeout).subscribe((data) => {\n' +
    '      this.toastr.success(\'Данные получены (вывод сообщения инициируется в методе компонента UnsubscribeComponent)\');\n' +
    '      this.data = data;\n' +
    '    });\n' +
    '  }';


  /** данные полученные с бакенда */
  public data = null;

  /** время через которое сервер ответит */
  public responseTimeout = 3000;

  private subs = new SubSink();


  public scenarios = {
    leave: () => {
      this.responseTimeout = 6000;
      this.getData();
      setTimeout(() => {
        this.goToHome();
      }, 2000);
    },
    leaveAndAgain: () => {
      this.responseTimeout = 6000;
      this.getData();
      setTimeout(() => {
        this.goToHome();
      }, 2000);
      setTimeout(() => {
        this.goToCurrentPage();
      }, 3000);
    },
    clicker: () => {
      this.responseTimeout = 2000;
      this.getData();
      setTimeout(() => {
        for (let i = 0; i < 4; i++) {
          this.getData();
        }
      }, 2000);
      setTimeout(() => {
        this.goToHome();
      }, 4500);
    }
  };

  getData(): void {
    this.data = null;
    this.subs.sink = this.dataService.getDataRx(this.responseTimeout).subscribe((data) => {
      this.toastr.success('Данные получены (вывод сообщения инициируется в методе компонента UnsubscribeComponent)');
      this.data = data;
    });
  }


  constructor(private dataService: DataService,
              private toastr: ToastrService,
              private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.toastr.warning('сработал ngOnDestroy компонента UnsubscribeComponent!');
  }

  goToHome(): void {
    this.router.navigate(['/']);
  }

  goToCurrentPage(): void {
    this.router.navigate(['/unsubscribe']);
  }

}
