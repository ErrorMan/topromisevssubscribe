import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-promise',
  templateUrl: './promise-await.component.html'
})
export class PromiseAwaitComponent implements OnInit, OnDestroy {

  public serviceCode = '  getDataPromise(timeout = 0): Promise<any> {\n' +
    '    return this.http.get(`${this.baseUrl}/api/mock-data?timeout=${timeout}`).toPromise();\n' +
    '  }';

  public scenariosCode = {
    leave: '() => {\n' +
      '      this.responseTimeout = 6000;\n' +
      '      this.getData();\n' +
      '      setTimeout(() => {\n' +
      '        this.goToHome();\n' +
      '      }, 2000);\n' +
      '    }',
    leaveAndAgain: '() => {\n' +
      '      this.responseTimeout = 6000;\n' +
      '      this.getData();\n' +
      '      setTimeout(() => {\n' +
      '        this.goToHome();\n' +
      '      }, 2000);\n' +
      '      setTimeout(() => {\n' +
      '        this.goToCurrentPage();\n' +
      '      }, 3000);\n' +
      '    }',
    clicker: '() => {\n' +
      '      this.responseTimeout = 2000;\n' +
      '      this.getData();\n' +
      '      setTimeout(() => {\n' +
      '        for (let i = 0; i < 4; i++) {\n' +
      '          this.getData();\n' +
      '        }\n' +
      '      }, 2000);\n' +
      '      setTimeout(() => {\n' +
      '        this.goToHome();\n' +
      '      }, 4000);\n' +
      '    }'
  };

  public getDataCode = '  async getData(): Promise<void> {\n' +
    '    this.data = null;\n' +
    '    this.data = await this.dataService.getDataPromise(this.responseTimeout);\n' +
    '    this.toastr.success(\'Данные получены (вывод сообщения инициируется в методе компонента PromiseAwaitComponent)\');\n' +
    '    console.log(\'это отобразится в консоли\');\n' +
    '    console.log(\'а еще у нас есть ссылка в памяти на экземпляр компонента\', this);\n' +
    '  }';

  /** данные полученные с бакенда */
  public data = null;

  /** время через которое сервер ответит */
  public responseTimeout = 3000;

  public scenarios = {
    leave: () => {
      this.responseTimeout = 6000;
      this.getData();
      setTimeout(() => {
        this.goToHome();
      }, 2000);
    },
    leaveAndAgain: () => {
      this.responseTimeout = 6000;
      this.getData();
      setTimeout(() => {
        this.goToHome();
      }, 2000);
      setTimeout(() => {
        this.goToCurrentPage();
      }, 3000);
    },
    clicker: () => {
      this.responseTimeout = 2000;
      this.getData();
      setTimeout(() => {
        for (let i = 0; i < 4; i++) {
          this.getData();
        }
      }, 2000);
      setTimeout(() => {
        this.goToHome();
      }, 4500);
    }
  };


  constructor(private dataService: DataService,
              private toastr: ToastrService,
              private router: Router
  ) {
  }

  ngOnDestroy(): void {
    this.toastr.warning('сработал ngOnDestroy компонента PromiseAwaitComponent!');
  }

  ngOnInit(): void {
  }

  async getData(): Promise<void> {
    this.data = null;
    this.data = await this.dataService.getDataPromise(this.responseTimeout);
    this.toastr.success('Данные получены (вывод сообщения инициируется в методе компонента PromiseAwaitComponent)');
    console.log('это отобразится в консоли');
    console.log('а еще у нас есть ссылка в памяти на экземпляр компонента', this);
  }

  goToHome(): void {
    this.router.navigate(['/']);
  }

  goToCurrentPage(): void {
    this.router.navigate(['/promise-await']);
  }


}
