import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private baseUrl = `${window.location.protocol}//${window.location.hostname}:3000`;

  constructor(private http: HttpClient) {
  }

  getDataRx(timeout = 0): Observable<any> {
    return this.http.get(`${this.baseUrl}/api/mock-data?timeout=${timeout}`);
  }

  getDataPromise(timeout = 0): Promise<any> {
    return this.http.get(`${this.baseUrl}/api/mock-data?timeout=${timeout}`).toPromise();
  }

}
