import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HIGHLIGHT_OPTIONS, HighlightModule } from 'ngx-highlightjs';
import { PromiseAwaitComponent } from './pages/promise-await/promise-await.component';
import { UnsubscribeComponent } from './pages/unsubscribe/unsubscribe.component';
import { HomeComponent } from './pages/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';
import { AsyncPipeComponent } from './pages/async-pipe/async-pipe.component';

@NgModule({
  declarations: [
    AppComponent,
    PromiseAwaitComponent,
    UnsubscribeComponent,
    HomeComponent,
    AsyncPipeComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HighlightModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatExpansionModule
  ],
  providers: [
    {
      provide: HIGHLIGHT_OPTIONS,
      useValue: {
        coreLibraryLoader: () => import('highlight.js/lib/core'),
        lineNumbersLoader: () => import('highlightjs-line-numbers.js'), // Optional, only if you want the line numbers
        languages: {
          typescript: () => import('highlight.js/lib/languages/typescript'),
          css: () => import('highlight.js/lib/languages/css'),
          xml: () => import('highlight.js/lib/languages/xml')}
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
